#!/usr/bin/env python3
# -*-coding:Utf-8 -*

"""
flexi_splitter library
"""

name = "flexi_splitter"
__all__ = ["flexi_splitter", "suffix_tree"]
