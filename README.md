# Flexi-Splitter : a flexible tool to sort reads into different fastq files from a barcode list and to manage barcodes and UMI in fastq files

Flexi-Splitter is a versatile tool designed for efficiently organizing reads into distinct FASTQ files based on a given barcode list. It also offers comprehensive management of barcodes and Unique Molecular Identifiers (UMIs) within FASTQ files.

## Dependencies
Python3 dependencies :
- biopython
- pyyaml

## Install


The simplest method to install flexi-splitter is using pip :

``` sh
pip3 install flexi-splitter
```

## How use it
``` sh
usage : flexi_splitter -f <inputfile1[,inputfile2...,inputfileN]> -o <outputfolder> -c <configfile> [OPTIONS]

mandatory parameters :
        -f, --fastqs   list of fastq files separated by a comma without whitespace
        -o, --ofolder  path to the output folder. Need to be empty or if not
                       exists, flexi_splitter creates it
        -c, --config   path to the configuration file in yaml format

options :
        -h, --help     print this message
        -v, --verbose  only usefull for debugging
        -m, --mismatch mismatches allowed per adaptor. By default, n-1
                       mismatches are allowed (n = adaptor length)
        -n, --ntuple   number of matched files (1 for single-end, 2 for paired-end, 3 if you have 3 matched files, etc...).
                       If not set, flexi_splitter will attemp to infer it
                       to guess it from the config file
        --version      print flexi_splitter version
```

## Config File

The config file is in yaml format and allows you to specify the number of barcodes you require. Here's an example of a config file:


``` sh
BCD:
  coords:
    reads: 1
    start: 7
    stop: 14
    header: False
  samples:
    - name : BCD1
      seq : TAGTGCC
    - name : BCD2
      seq : GTTAGCC
BCD:
  coords:
    reads: 2
    start: 1
    stop: 6
    header: True
  samples:
    - name : BCD3
      seq: ATCACG
    - name : BCD4
      seq : CGATGT
UMI:
  coords:
    reads: 1
    start: 1
    stop: 6
    header: False
conditions:
  sample1:
    - BCD1
    - BCD3
  sample2:
    - BCD2
    - BCD4
```

### BDC section

In this example, two barcodes, "BCD1" and "BCD2," are specified. Each barcode contains a "coords" section with the following information:
- **reads** : file that contain the barcode i.e. in  paired-end sequencing in can be in the first file (1) or in the second file (2)
- **start** : position of the first barcode nucleotide in the sequence
- **stop** : position of the last barcode nucleotide in the sequence
- **header** : A boolean value. In some cases, barcodes are placed in the header of each read. If so, the position of the barcode is relative to the end of the header. For example, start = 1 and stop = 6 means that the barcode is in the last 6 characters of the header

Barcodes sections must contain also samples subsection, listing all existing barcodes sequences. You can name the barcode as you whish but it the name must be unique within the file.

### UMI
If the barcodes is named 'UMI', there is no 'samples' subsection, as UMI are random and thus unknown. If 'UMI' is spceficied, the sequence is paste at the end of the first field in the header of the reads, separated by '_'. This allows it to be later used with UMItools for demultiplexing. In all cases, barcodes are removed from the sequence of the read.

### Conditions
In this section, all experimental samples and their barcode combinations are listed.

## Output

In the output folder, flexi-splitter creates one subfolder per experimental sample. Each folder contains the same number of files as specified with the -f parameter.

## Contributing

Install in development mode:

```console
$ cd flexi_splitter
$ python3 -m venv venv
$ source venv/bin/activate
$ pip install src/
$ pre-commit install
```
